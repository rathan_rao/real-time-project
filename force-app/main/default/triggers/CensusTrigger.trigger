trigger CensusTrigger on Census_Member__c (after insert, after update, before insert, before update) 
{
    if(Trigger.isAfter &&  Trigger.isInsert)
    {
        CensusHandler.onAfterInsert(Trigger.New);
    }
    if( Trigger.isAfter && Trigger.isUpdate )
    {
        CensusHandler.onAfterUpdate(Trigger.NewMap, Trigger.OldMap);
    }
    if( Trigger.isBefore && Trigger.isInsert)
    {
        CensusHandler.onBeforeInsert(Trigger.New);
    }
    if( Trigger.isBefore && Trigger.isUpdate)
    {
        CensusHandler.onBeforeUpdate(Trigger.NewMap, Trigger.OldMap);
    }

}