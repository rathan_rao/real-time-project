@isTest public without sharing class JewelryHandler_Test 
{
    @isTest public static void  testOnBeforeInsert() 
    {
        List<Jewelry__c> lstJwl = new List<Jewelry__c>();
        for( Integer i = 1; i <= 10 ; i++ )
        {
            lstJwl.add ( new Jewelry__c( Name = ' Diamond' +i, Price__c = 100 , Quantity__c = 1 ));
        }
        
        Test.startTest();
        insert lstJwl;
        Test.stopTest();
        System.debug('Size:' + lstJwl.size());
        System.assertEquals(10,lstJwl.size() );
        List<Jewelry__c> lstJwlVer = [SELECT Name, Price__c, Quantity__c, Total_Price__c FROM Jewelry__c ];
        System.debug('Total Price:' + lstJwlVer[0].Total_Price__c);
        System.assertEquals(100,lstJwlVer[0].Total_Price__c );
    }
    @isTest public static void  testOnBeforeUpdate()
    {
        List<Jewelry__c> lstJwlUpdate = new List<Jewelry__c>{new Jewelry__c(Name = 'Pearl', Price__c = 200 , Quantity__c = 3)} ;
        insert lstJwlUpdate;
        System.debug('Size:' + lstJwlUpdate.size());
        System.assertEquals(1,lstJwlUpdate.size() );

        lstJwlUpdate[0].Price__c = 300 ;
        lstJwlUpdate[0].Quantity__c = 4 ;

        Test.startTest();
        update lstJwlUpdate;
        Test.stopTest();

        List<Jewelry__c> lstJwlUpdateVer = [SELECT Name, Price__c, Quantity__c, Total_Price__c FROM Jewelry__c ];
        System.debug('Total Price:' + lstJwlUpdateVer[0].Total_Price__c);
        System.assertEquals(1200,lstJwlUpdateVer[0].Total_Price__c );




    }
}