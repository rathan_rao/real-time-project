@isTest public without sharing class CensusHandler_Test 
{
     @TestSetup public static void setup()
    {
        Account objAcc = new Account( Name = ' New Account ');
        insert objAcc;
        Date cnEffDate = Date.newInstance(2021, 1, 1);
        Date cnEndDate = Date.newInstance(2021, 12, 30);
        Census__c objCen = new Census__c ( Name =' New Census Object ' ,vlocity_ins_GroupId__c = objAcc.Id, vlocity_ins_EffectiveStartDate__c = cnEffDate, vlocity_ins_EffectiveEndDate__c = cnEndDate);
        insert objCen;

    }

    ///----------------------------TASK 5 -------------------------------///
    @isTest public static void  testOnInsertPrimaryFieldOnPrimaryCheck() 
    {
        Census__c objCenNew = [SELECT Id, Name, vlocity_ins_GroupId__c FROM Census__c  ];
        Census_Member__c objCm = new Census_Member__c( Name = ' Rathan ', vlocity_ins_IsPrimaryMember__c = TRUE , vlocity_ins_CensusId__c = objCenNew.Id);
        Test.startTest();
        insert objCm;
        Test.stopTest();

        Census__c lstCenVer = [SELECT Name, Primary_Member__c FROM Census__c];
        System.assertEquals('Rathan', lstCenVer.Primary_Member__c);

    }

    @isTest public static void  testOnUpdatePrimaryFieldonPrimaryCheck() 
    {
        Census__c objCenNew = [SELECT Id, Name, vlocity_ins_GroupId__c FROM Census__c  ];
        Census_Member__c objCmUpdate = new Census_Member__c( Name = 'Rathan 2', vlocity_ins_IsPrimaryMember__c = FALSE , vlocity_ins_CensusId__c = objCenNew.Id);
        insert objCmUpdate;

        objCmUpdate.vlocity_ins_IsPrimaryMember__c = TRUE;
        update objCmUpdate;

        Census__c lstCenUpdateVerify = [SELECT Name, Primary_Member__c FROM Census__c];
        System.assertEquals('Rathan 2', lstCenUpdateVerify.Primary_Member__c);

        Census_Member__c objCmUpdateTwo = [SELECT Name,vlocity_ins_IsPrimaryMember__c FROM Census_Member__c ];
        objCmUpdateTwo.vlocity_ins_IsPrimaryMember__c = FALSE;

        Test.startTest();
        update objCmUpdateTwo;
        Test.stopTest();

        Census__c lstCenUpdateVer = [SELECT Name, Primary_Member__c FROM Census__c];
        System.assertEquals( NULL , lstCenUpdateVer.Primary_Member__c);

    }

    ///----------------------------TASK 6 -------------------------------///
    @isTest public static void  testOnThrowErrorOnDateInsert()
    {
        try {
            Census__c objCenNew = [SELECT Id, Name, vlocity_ins_GroupId__c FROM Census__c  ];
            Date cmEffDate = Date.newInstance(2021, 1, 3);
            Date cmTermDate = Date.newInstance(2023, 11, 22);
            Census_Member__c objCmNew2 = new Census_Member__c( Name = ' Rathan 3 ', vlocity_ins_IsPrimaryMember__c = FALSE , vlocity_ins_CensusId__c = objCenNew.Id, htcs_EffectiveDate__c = cmEffDate, htcs_TermDate__c = cmTermDate);
            insert objCmNew2;
            
            } 
            catch (Exception e) 
            {
                Boolean errorMsgOnInsert =  e.getMessage().contains('Edit the Dates') ? true : false;
                System.assertEquals(errorMsgOnInsert,true);
            }

    }

    @isTest public static void  testOnThrowErrorOnDateUpdate()
    {
        try {
            Census__c objCenNew = [SELECT Id, Name, vlocity_ins_GroupId__c FROM Census__c  ];
            Date cmEffDate = Date.newInstance(2021, 1, 3);
            Date cmTermDate = Date.newInstance(2021, 11, 22);
            Census_Member__c objCmNew3 = new Census_Member__c( Name = ' Rathan 4 ', vlocity_ins_IsPrimaryMember__c = FALSE , vlocity_ins_CensusId__c = objCenNew.Id, htcs_EffectiveDate__c = cmEffDate, htcs_TermDate__c = cmTermDate);
            insert objCmNew3;

            Census_Member__c ver = [SELECT Name, htcs_EffectiveDate__c , htcs_TermDate__c FROM Census_Member__c WHERE Name = 'Rathan 4' ];

            Date cmEffDateUpdate = Date.newInstance(2020, 1, 3);
            Date cmTermDateUpdate = Date.newInstance(2024, 11, 22);

            ver.htcs_EffectiveDate__c = cmEffDateUpdate;
            ver.htcs_TermDate__c = cmTermDateUpdate;
            
            update ver;

            System.debug(ver);
            //Census_Member__c ver = [SELECT Name, htcs_EffectiveDate__c , htcs_TermDate__c FROM Census_Member__c WHERE Name = 'Rathan 4' ];
           // System.debug(ver);

            
        } 
        catch (Exception e ) 
        {
            Boolean errorMsgOnUpdate =  e.getMessage().contains('Edit the Dates ( UPDATE )') ? true : false;
            System.assertEquals(errorMsgOnUpdate,true);
        }
        

        

    }

    ///----------------------------TASK 7.1 -------------------------------///
    @isTest public static void  testOnThrowErrorOnPrimaryMemberLimitExceedingDuringInsert()
     {
            try 
            {
                Census__c objCenNew = [SELECT Id, Name, vlocity_ins_GroupId__c FROM Census__c  ];
                Census_Member__c objCmNew = new Census_Member__c( Name = ' Rathan 5 ', vlocity_ins_IsPrimaryMember__c = TRUE , vlocity_ins_CensusId__c = objCenNew.Id);
                insert objCmNew;

                Census_Member__c objCmNew2 = new Census_Member__c( Name = ' Rathan 6 ', vlocity_ins_IsPrimaryMember__c = TRUE , vlocity_ins_CensusId__c = objCenNew.Id);
                insert objCmNew2;

            } 
            catch (Exception e) 
            {
                Boolean errorOnInsert = e.getMessage().contains('Selected Census already has a Primary Census Member');
                System.assertEquals(errorOnInsert, true);   
            }

    }

    @isTest public static void  testOnThrowErrorOnPrimaryMemberLimitExceedingDuringUpdate()
    {
        try 
        {
            Census__c objCenNew = [SELECT Id, Name, vlocity_ins_GroupId__c FROM Census__c  ];
            Census_Member__c objCmNew3 = new Census_Member__c( Name = ' Rathan 7 ', vlocity_ins_IsPrimaryMember__c = TRUE , vlocity_ins_CensusId__c = objCenNew.Id);
            insert objCmNew3;

            Census_Member__c objCmNew4 = new Census_Member__c( Name = ' Rathan 8 ', vlocity_ins_IsPrimaryMember__c = FALSE , vlocity_ins_CensusId__c = objCenNew.Id);
            insert objCmNew4;

            Census_Member__c objCenVer = [SELECT Name, vlocity_ins_IsPrimaryMember__c, vlocity_ins_CensusId__c FROM Census_Member__c WHERE Name = 'Rathan 8'];
            objCenVer.vlocity_ins_IsPrimaryMember__c = TRUE ;
            update objCenVer;   
        } 
        catch (Exception e) 
        {
            Boolean errorOnUpdate = e.getMessage().contains('Selected Census already has a Primary Census Member');
            System.assertEquals(errorOnUpdate, true); 
            
        }
    }

    ///----------------------------TASK 7.2 -------------------------------///
    @isTest public static void  testOnAppendCensusMemberNamesDuringInsert()
    {
            Census__c objCenNew = [SELECT Id, Name, vlocity_ins_GroupId__c FROM Census__c  ];
            List<Census_Member__c> lstCm = new List<Census_Member__c>();
            

            /*for( Integer i =5 ; i <= 5 ; i++)
            {
                lstCm.add( new Census_Member__c(Name = 'Rathan ' +i, vlocity_ins_IsPrimaryMember__c = FALSE , vlocity_ins_CensusId__c = objCenNew.Id, htcs_EffectiveDate__c = cmEffDate, htcs_TermDate__c = cmTermDate ));
            }*/

            //System.debug(lstCm);
           Census_Member__c objCmNew5 = new Census_Member__c( Name = 'Rathan 9', vlocity_ins_IsPrimaryMember__c = FALSE , vlocity_ins_CensusId__c = objCenNew.Id);
            insert objCmNew5;

            Census_Member__c objCmNew6 = new Census_Member__c( Name = 'Rathan 10', vlocity_ins_IsPrimaryMember__c = FALSE , vlocity_ins_CensusId__c = objCenNew.Id);

            Test.startTest();
            insert objCmNew6;
            Test.stopTest();

            Census__c objCenVerify = [SELECT Id, Name, vlocity_ins_GroupId__c, Members__c FROM Census__c ];
            System.assertEquals('Rathan 9 ; Rathan 10', objCenVerify.Members__c); 
    } 

    @isTest public static void  testOnAppendCensusMemberNamesDuringUpdate()
    {
            Census__c objCenNew = [SELECT Id, Name, vlocity_ins_GroupId__c FROM Census__c  ];
            List<Census_Member__c> lstCm = new List<Census_Member__c>();
            for( Integer i =1 ; i <= 5 ; i++)
            {
                lstCm.add( new Census_Member__c(Name = 'CM ' +i, vlocity_ins_IsPrimaryMember__c = FALSE , vlocity_ins_CensusId__c = objCenNew.Id ));
            }

            insert lstCm;
            List<Census__c> lstCenVerY = [SELECT Name, Members__c FROM Census__c];
            System.debug('---------------------BEFORE UPDATE -----------------------------------');
            System.debug(lstCenVerY);

            List<Census_Member__c> lstCmVeryy = [SELECT Name FROM Census_Member__c ];

           for ( Integer i = 0 ; i <= 4 ; i++ )
            {
                lstCmVeryy[i].Name = 'New CM  ' +i ;
            }

            System.debug(lstCmVeryy);
            Test.startTest();
            update lstCmVeryy;
            Test.stopTest();
            
            List<Census__c> lstCenVer = [SELECT Members__c FROM Census__c LIMIT 1];
            System.debug('---------------------AFTER UPDATE -----------------------------------');
            System.debug(lstCenVer);
            System.assertEquals('New CM 0 ; New CM 1 ; New CM 2 ; New CM 3 ; New CM 4',lstCenVer[0].Members__c );

    } 





}