public without sharing class CensusHandler 
{


    public static void onAfterInsert(List<Census_Member__c> lstCm)
    {
        CensusHandler.insertPrimaryFieldOnPrimaryCheck(lstCm);
        CensusHandler.appendCensusMemberNamesDuringInsert(lstCm);
    }

    public static void onAfterUpdate( Map<Id,Census_Member__c> mapCmNew, Map<Id, Census_Member__c> mapCmOld )
    {
        CensusHandler.updatePerimaryFieldonPrimaryCheck(mapCmNew,mapCmOld);
        CensusHandler.appendCensusMemberNamesDuringUpdate(mapCmNew,mapCmOld);
    }

    public static void onBeforeInsert(List<Census_Member__c> lstCenMem)
    {
        CensusHandler.throwErrorOnDateInsert(lstCenMem);
        CensusHandler.throwErrorOnPrimaryMemberLimitExceedingDuringInsert(lstCenMem);
    }

    public static void onBeforeUpdate(Map<Id,Census_Member__c> mapCmNewUp, Map<Id, Census_Member__c> mapCmOldUp)
    {
        CensusHandler.throwErrorOnDateUpdate(mapCmNewUp,mapCmOldUp);
        CensusHandler.throwErrorOnPrimaryMemberLimitExceedingDuringUpdate(mapCmNewUp,mapCmOldUp);
    }


    /// --------------------------------------TASK 5 ------------------------------------------------------------------------///
    /*
        @ Method name        :   insertPrimaryFieldOnPrimaryCheck
        @ Created by        :   Rathan
        @ Created on        :   12-29-2021
        @ jira ticket       : SLVI -060
        @ Description       :  When Primary Checkbox is ticked in Census Member then Fill the Primary Member Field in Census with the respective Census Member Name. 
    */
    public static void insertPrimaryFieldOnPrimaryCheck(List<Census_Member__c> lstCm)  //After Insert
    {
        List<Census__c> lstCen = new List<Census__c>();
        for(Census_Member__c objCm : lstCm )
        {
            if( objCm.vlocity_ins_IsPrimaryMember__c == True )
            {
                Census__c objCen = new Census__c();
                objCen.Id = objCm.vlocity_ins_CensusId__c;
                objCen.Primary_Member__c = objCm.Name;
                lstCen.add(objCen);
            }
        }
        if(!lstCen.isEmpty())
        {
            update lstCen;
        }

    }

    /*
        @ Method name        :   updatePerimaryFieldonPrimaryCheck
        @ Created by        :   Rathan
        @ Created on        :   12-29-2021
        @ jira ticket       : SLVI -060
        @ Description       :  When Primary Checkbox is Updated from Unchecked to Checked  in Census Member then Fill the Primary Member Field in Census with the respective Census Member Name. 
    */
    public static void updatePerimaryFieldonPrimaryCheck(Map<Id, Census_Member__c> mapCmNew , Map<Id, Census_Member__c> mapCmOld) // After Update
    {
        List<Census__c> lstCenUpdate = new List<Census__c>();
        for( Id cmId : mapCmNew.keyset())
        {
            Census_Member__c objCmNew = mapCmNew.get(cmId);
            Census_Member__c objCmOld = mapCmOld.get(cmId);
            if( objCmNew.vlocity_ins_IsPrimaryMember__c != objCmOld.vlocity_ins_IsPrimaryMember__c && objCmNew.vlocity_ins_IsPrimaryMember__c == TRUE )
            {
                Census__c objCenUpdate = new Census__c( Id = objCmNew.vlocity_ins_CensusId__c , Primary_Member__c = objCmNew.Name );   
                lstCenUpdate.add(objCenUpdate);
            }
            else if (objCmNew.vlocity_ins_IsPrimaryMember__c != objCmOld.vlocity_ins_IsPrimaryMember__c && objCmNew.vlocity_ins_IsPrimaryMember__c == FALSE )
            {
                Census__c objCenUpdate = new Census__c( Id = objCmNew.vlocity_ins_CensusId__c,  Primary_Member__c = '' );
                lstCenUpdate.add(objCenUpdate);
            } 


        }
        if(!lstCenUpdate.isEmpty())
        {
            update lstCenUpdate;
        }


    }

    /// --------------------------------------TASK 6 ------------------------------------------------------------------------///
    /*
        @ Method name        :   throwErrorOnDateInsert
        @ Created by        :   Rathan
        @ Created on        :   12-29-2021
        @ jira ticket       : SLVI -060
        @ Description       :  When a related Census Member is created , the Effective Date and Term date should fall in between the Effective Date and End Date of the Census. 
    */
    public static void throwErrorOnDateInsert(List<Census_Member__c> lstCenMem)  // Before Insert
    {
        Set<Id> setCmId = new Set<Id>();
        for( Census_Member__c objCenRec : lstCenMem)
        {
            setCmId.add(objCenRec.vlocity_ins_CensusId__c);
        }
        //System.debug(setCmId);
        Map<Id, Census__c> mapCen = new Map<Id, Census__c>([SELECT Id, Name, vlocity_ins_EffectiveStartDate__c, vlocity_ins_EffectiveEndDate__c 
        FROM Census__c WHERE Id IN :setCmId ]) ;

        //System.debug(mapCen);
       
        for( Census_Member__c objCenMem : lstCenMem )
        {
            if(mapCen.get(objCenMem.vlocity_ins_CensusId__c).vlocity_ins_EffectiveStartDate__c != NULL || mapCen.get(objCenMem.vlocity_ins_CensusId__c).vlocity_ins_EffectiveEndDate__c != NULL )
            {
               // System.debug(mapCen.get(objCenMem.vlocity_ins_CensusId__c).vlocity_ins_EffectiveStartDate__c + '  ------------------------  '  + mapCen.get(objCenMem.vlocity_ins_CensusId__c).vlocity_ins_EffectiveEndDate__c );
                if( objCenMem.htcs_EffectiveDate__c <=  mapCen.get(objCenMem.vlocity_ins_CensusId__c).vlocity_ins_EffectiveStartDate__c 
                || objCenMem.htcs_TermDate__c >= mapCen.get(objCenMem.vlocity_ins_CensusId__c).vlocity_ins_EffectiveEndDate__c)
                {
                    objCenMem.addError('Edit the Dates');
                }
            }
        }


    }

    /*
        @ Method name        :   throwErrorOnDateUpdate
        @ Created by        :   Rathan
        @ Created on        :   12-29-2021
        @ jira ticket       : SLVI -060
        @ Description       :  When a related Census Member's  Effective Date and Term date is updated then respective dates should fall in between the Effective Date and End Date of the Census or it will throw an error. 
    */
    public static void throwErrorOnDateUpdate(Map<Id, Census_Member__c> mapCmNewUp , Map<Id, Census_Member__c> mapCmOldUp) // Before Update
    {
        Set<Id> setCmId = new Set<Id>();
        for( Census_Member__c objCm : mapCmNewUp.values())
        {
            setCmId.add(objCm.vlocity_ins_CensusId__c);
        }
        Map<Id, Census__c> mapCen = new Map<Id, Census__c>([SELECT Id, Name, vlocity_ins_EffectiveStartDate__c, vlocity_ins_EffectiveEndDate__c FROM Census__c WHERE Id IN : setCmId]) ;

        for( Id cmId : mapCmNewUp.keySet() )
        { 
            if( mapCen.get(mapCmNewUp.get(cmId).vlocity_ins_CensusId__c).vlocity_ins_EffectiveStartDate__c != NULL|| mapCen.get(mapCmNewUp.get(cmId).vlocity_ins_CensusId__c).vlocity_ins_EffectiveEndDate__c != NULL)
            {
                if( mapCmNewUp.get(cmId).htcs_EffectiveDate__c <= (mapCen.get(mapCmNewUp.get(cmId).vlocity_ins_CensusId__c).vlocity_ins_EffectiveStartDate__c) 
                || mapCmNewUp.get(cmId).htcs_TermDate__c >= mapCen.get(mapCmNewUp.get(cmId).vlocity_ins_CensusId__c).vlocity_ins_EffectiveEndDate__c )
                {
                    mapCmNewUp.get(cmId).addError('Edit the Dates ( UPDATE )');
                }
            }
            
        }
    }

    /// --------------------------------------TASK 7.1 ------------------------------------------------------------------------///
    /*
        @ Method name        :   throwErrorOnPrimaryMemberLimitExceedingDuringInsert
        @ Created by        :   Rathan
        @ Created on        :   12-29-2021
        @ jira ticket       : SLVI -060
        @ Description       :  There should be one relate Primary Census Member for a Census Record or it will thrown an error. 
    */
    public static void throwErrorOnPrimaryMemberLimitExceedingDuringInsert(List<Census_Member__c> lstCenMem)  // Before Insert
    {
        Set<Id> setCmId = new Set<Id>();
        for(Census_Member__c objCenMem : lstCenMem)
        {
            if( objCenMem.vlocity_ins_IsPrimaryMember__c == True  )
            {
                setCmId.add( objCenMem.vlocity_ins_CensusId__c );
            }
        }

        List<Census__c> lstCensus = [SELECT Id, Name, (SELECT  vlocity_ins_CensusId__c, Name FROM vlocity_ins_CensusId__r WHERE vlocity_ins_IsPrimaryMember__c = TRUE ) FROM Census__c WHERE Id IN :setCmId ];
        Map<Id,Boolean> mapOfCenHavingCenMem = new  Map<Id,Boolean>();
        for( Census__c objCensus :  lstCensus)
        {
            if( objCensus.vlocity_ins_CensusId__r.size() == 1)
            {
                mapOfCenHavingCenMem.put(objCensus.Id, true);
            }
        }
        for( Census_Member__c objCenMember : lstCenMem )
        {
            if( mapOfCenHavingCenMem.containsKey(objCenMember.vlocity_ins_CensusId__c) )
            {
                objCenMember.addError('Selected Census already has a Primary Census Member');
            }
        }
    }

    /*
        @ Method name        :   throwErrorOnPrimaryMemberLimitExceedingDuringUpdate
        @ Created by        :   Rathan
        @ Created on        :   12-30-2021
        @ jira ticket       : SLVI -060
        @ Description       :  There should be one relate Primary Census Member for a Census Record or it will thrown an error. 
    */
    public static void throwErrorOnPrimaryMemberLimitExceedingDuringUpdate(Map<Id,Census_Member__c> mapCmNewUp, Map<Id, Census_Member__c> mapCmOldUp )  // Before Update
    {
        Set<Id> setId = new Set<Id>();
        for( Census__c objCensusNew : [SELECT Id FROM Census__c WHERE Primary_Member__c != NULL])
        {
            setId.add(objCensusNew.Id);
        }

        for(Id cmId : mapCmNewUp.keySet())
        {
            Census_Member__c objCenMemNew = mapCmNewUp.get(cmId);
            Census_Member__c objCenMemOld = mapCmOldUp.get(cmId);
            if( setId.contains( mapCmNewUp.get(cmId).vlocity_ins_CensusId__c) && (objCenMemNew.vlocity_ins_IsPrimaryMember__c != objCenMemOld.vlocity_ins_IsPrimaryMember__c ) && objCenMemNew.vlocity_ins_IsPrimaryMember__c == TRUE  )
            {
                mapCmNewUp.get(cmId).addError('Selected Census already has a Primary Census Member( Update ) ');
            }

        }

    }

    /// --------------------------------------TASK 7.2 ------------------------------------------------------------------------///
    /*
        @ Method name        :   appendCensusMemberNamesDuringInsert
        @ Created by        :   Rathan
        @ Created on        :   12-30-2021
        @ jira ticket       : SLVI -060
        @ Description       :  When Multiple Related Census Members are created then the Name of the Census Members must be Inserted into the Members field 
                               in Census Record and the names  should be seperated by Semicolon . 
    */
    public static void appendCensusMemberNamesDuringInsert( List<Census_Member__c> lstCm )  // After Insert
    {
        List<Census__c> lstCenIns = new List<Census__c>();
        Set<Id> setCmId = new Set<Id>();
        for( Census_Member__c objCenRec : lstCm)
        {
            setCmId.add(objCenRec.vlocity_ins_CensusId__c);
        }

        Map<Id, Census__c> mapCenRec = new Map<Id, Census__c>([SELECT Id, Name, vlocity_ins_EffectiveStartDate__c, vlocity_ins_EffectiveEndDate__c,Members__c FROM Census__c WHERE Id IN : setCmId]) ;
        for( Census_Member__c objCenMemInsert : lstCm )
        {
            if(mapCenRec.get(objCenMemInsert.vlocity_ins_CensusId__c).Members__c != NULL)
            {
                if( objCenMemInsert.vlocity_ins_CensusId__c == mapCenRec.get(objCenMemInsert.vlocity_ins_CensusId__c).Id )
                {
                    mapCenRec.put(objCenMemInsert.vlocity_ins_CensusId__c, new Census__c(Id = objCenMemInsert.vlocity_ins_CensusId__c , Members__c = mapCenRec.get(objCenMemInsert.vlocity_ins_CensusId__c).Members__c + ' ; ' + objCenMemInsert.Name ) );   

                }
            }
            else 
            {
                mapCenRec.put(objCenMemInsert.vlocity_ins_CensusId__c, new Census__c(Id = objCenMemInsert.vlocity_ins_CensusId__c , Members__c =  objCenMemInsert.Name ) );
            }
            
        }

        if(!mapCenRec.isEmpty())
        {
            update mapCenRec.values();
        }      
        
    }

    /*
        @ Method name        :   appendCensusMemberNamesDuringUpdate
        @ Created by        :   Rathan
        @ Created on        :   12-30-2021
        @ jira ticket       : SLVI -060
        @ Description       :  When a Related Census Member's Name is edited to some other name then the Name should be updated in the Member's Field in the Related
                               Census Record . 
    */
    public static void appendCensusMemberNamesDuringUpdate( Map<Id,Census_Member__c> mapCmNew, Map<Id, Census_Member__c> mapCmOld ) // After Update
    {
        List<Census__c> lstCenUpdate = new List<Census__c>();
        Set<Id> setCmId = new Set<Id>();
        for( Census_Member__c objCm : mapCmNew.values())
        {
            setCmId.add(objCm.vlocity_ins_CensusId__c);
        }

        Map<Id, Census__c> mapCenRec = new Map<Id, Census__c>([SELECT Id, Name,Members__c FROM Census__c WHERE Id IN : setCmId]) ;
         
        for( Id cenMemId : mapCmNew.keySet())
        {
            if( mapCenRec.get(mapCmNew.get(cenMemId).vlocity_ins_CensusId__c).Members__c != NULL)
            {
                if( mapCmNew.get(cenMemId).vlocity_ins_CensusId__c == mapCenRec.get(mapCmNew.get(cenMemId).vlocity_ins_CensusId__c).Id ||  mapCmOld.get(cenMemId).Name != mapCmOld.get(cenMemId).Name )
                {
                    String strS1 = mapCenRec.get(mapCmNew.get(cenMemId).vlocity_ins_CensusId__c).Members__c ;
                    String strTarget = mapCmOld.get(cenMemId).Name;
                    String strReplacement = mapCmNew.get(cenMemId).Name;
                    String strUpdatedMem = strS1.replace(strTarget, strReplacement);
                    mapCenRec.put(mapCmNew.get(cenMemId).vlocity_ins_CensusId__c, new Census__c(Id = mapCmNew.get(cenMemId).vlocity_ins_CensusId__c , Members__c =  strUpdatedMem ) );
                }
            }

        }

        if (!mapCenRec.isEmpty())
        {
            update mapCenRec.values();
        }
    }


}