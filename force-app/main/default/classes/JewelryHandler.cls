public without sharing class JewelryHandler 
{

    public static void onBeforeInsert( List<Jewelry__c> lstJwl )
    {
        JewelryHandler.calculateTotalPriceOnInsert(lstJwl);
    }

    public static void onBeforeUpdate(Map<Id, Jewelry__c> mapJwlNew, Map<Id, Jewelry__c> mapJwlOld )
    {
        JewelryHandler.calculateTotalPriceOnUpdate( mapJwlNew,mapJwlOld );
    }

    /*
        @ Method name        :   calculateTotalPriceOnInsert
        @ Created by        :   Rathan
        @ Created on        :   12-29-2021
        @ jira ticket       : SLVI -060
        @ Description       :   Calculate Total Price when Price and Quantity Field is filled . 
    */
    public static void calculateTotalPriceOnInsert( List<Jewelry__c> lstJwl) 
    {
        List<Jewelry__c> lstJwlNew = new List<Jewelry__c>();
        for(Jewelry__c objJwl : lstJwl )
        {
            if(objJwl.Price__c != NULL && objJwl.Quantity__c != NULL )
            {
                objJwl.Total_Price__c =   objJwl.Price__c * objJwl.Quantity__c ;
                lstJwlNew.add(objJwl);

            }
        }
     
        
    }
    /*
        @ Method name        :   calculateTotalPriceOnUpdate
        @ Created by        :   Rathan
        @ Created on        :   12-29-2021
        @ jira ticket       : SLVI -060
        @ Description       :   Calculate Total Price when Price and Quantity Field is is updated with new values . 
    */
    public static void calculateTotalPriceOnUpdate( Map<Id, Jewelry__c> mapJwlNew, Map<Id, Jewelry__c> mapJwlOld )
    {
        for( Id jwlId : mapJwlNew.keyset())
        {
            Jewelry__c objJwlNew = mapJwlNew.get(jwlId);
            Jewelry__c objJwlOld = mapJwlOld.get(jwlId);
            if(objJwlNew.Id == objJwlOld.Id && objJwlNew.Price__c != objJwlOld.Price__c || objJwlNew.Quantity__c != objJwlOld.Quantity__c)
            {

                objJwlNew.Total_Price__c = objJwlNew.Price__c * objJwlNew.Quantity__c ; 

            }
        }
        

    }

}